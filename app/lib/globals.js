Titanium.include('constants.js');

exports.getAnimationDict = function(close) {
	if (isAndroid()) {
		var animationDict = {
			activityEnterAnimation : Ti.Android.R.anim.slide_in_left,
			activityExitAnimation : Ti.Android.R.anim.slide_out_right
		};

		return animationDict;
	} else {
		if (close) {
			var animationDict = {
				transition : Titanium.UI.iPhone.AnimationStyle.CURL_DOWN
			};
			return animationDict;
		} else {
			var animationDict = {
				transition : Titanium.UI.iPhone.AnimationStyle.CURL_UP
			};
			return animationDict;
		}
	}
};
function createCircleView(color, zIndex, rightMargin, diameter) {
	var radius = diameter / 2;
	var params = {
		width : diameter+ 'dp',
		height : diameter+ 'dp',
		borderRadius : radius + 'dp',
		borderColor : 'transparent',
		backgroundColor : color,
		color : color,
		_type : "centerImage",
		zIndex : zIndex,
	};
	if (rightMargin > 0) {
		params.right = rightMargin+ 'dp';
	}
	var circle = Ti.UI.createView(params);
	return circle;
}

exports.addImageOverlayToCenter = function(imageView) {

	var dotDiameter = 6;
	var rightMarin = 4;
	var circleDiameter = (4 * rightMarin) + (dotDiameter * 3);
	var circle = createCircleView('#80000000', 10, 0, circleDiameter);
	var dot1 = createCircleView('#fff', 12, rightMarin, dotDiameter);
	var dot2 = createCircleView('#fff', 12, rightMarin, dotDiameter);
	var dot3 = createCircleView('#fff', 12, rightMarin, dotDiameter);

	var dotsContainer = Ti.UI.createView({
		left : rightMarin+ 'dp',
		zIndex : 11,
		color : 'transparent',
		backgroundColor : 'transparent',
		layout : 'horizontal',
		width : Ti.UI.SIZE,
		height : Ti.UI.SIZE,
		_type : "centerImage"
	});

	dotsContainer.add(dot1);
	dotsContainer.add(dot2);
	dotsContainer.add(dot3);

	circle.add(dotsContainer);
	return circle;
};

exports.addTitleOverLayToImage = function(imageView, title) {
	var charsView = Ti.UI.createView({
		bottom : '0dp',
		zIndex : 2,
		width : Ti.UI.FILL,
		height : "25%",
		backgroundColor : '#80000000'
	});

	var charsName = Ti.UI.createLabel({
		bottom : '0dp',
		text : title,
		zIndex : 3,
		color : '#fff',
		width : Ti.UI.SIZE,
		height : "25%",
		backgroundColor : 'transparent',
		font : {
			fontSize : '18sp',
			fontWeight : 'bold'
		}
	});
	imageView.add(charsName);
	imageView.add(charsView);

};

exports.getTitleFromType = function(listType) {
	var sectionTitle;
	if (listType == "industries") {
		sectionTitle = "Industry";
	} else if (listType == "platforms") {
		sectionTitle = "Platform";
	} else if (listType == "technologies") {
		sectionTitle = "Technology";
	} else {
		sectionTitle = "All";
	}
	return sectionTitle;
};

exports.isEmptyDict = function(ob) {
	for (var i in ob) {
		return false;
	}
	return true;
};

exports.hideView = function(view) {
	hideView(view);
};

function hideView(view) {
	if (view != null) {
		view.visible = false;
		view.height = '0dp';
		view.width = '0dp';
		view.top = '0dp';
		view.bottom = '0dp';
		view.left = '0dp';
		view.right = '0dp';
	}
}

exports.initFilterSection = function(menuData, type) {
	var list = _.where(menuData, {
		type : type
	});

	var finalList = [];
	var macdata = require('macdata');
	for (var i = 0; i < list.length; i++) {
		var listItem = list[i];
		var menuItems = macdata.tagProjects(listItem.name);
		if (menuItems && menuItems.length > 0) {
			finalList.push(listItem);
		}
	}
	return finalList;
};

exports.getImageDimensions = function(filePath) {
	getImageDimensions(filePath);
};

exports.findChildViewById = function(view, id) {
	for (var x in view.children ) {
		if (view.children[x].id == id) {
			return view.children[x];
		}
	}
	return null;
};

exports.removeAllChildren = function(viewObject) {
	//copy array of child object references because view's "children" property is live collection of child object references
	var children = viewObject.children.slice(0);

	for (var i = 0; i < children.length; ++i) {
		viewObject.remove(children[i]);
	}
};

function getImageDimensions(filePath) {
	/*
	 * This method is supposed to be faster but couldn't get it to work for iOS.
	 *
	 */
	if (isAndroid()) {
		var testFile = Ti.Filesystem.getFile(filePath).read();
		var imgWidth = testFile.width;
		var imgHeight = testFile.height;

		return {
			width : imgWidth,
			height : imgHeight
		};
	}

	var imgView = Ti.UI.createImageView({
		image : filePath,
		width : "auto",
		height : "auto"
	});
	var imgWidth = imgView.toImage().width;
	var imgHeight = imgView.toImage().height;
	imgView = null;

	return {
		width : imgWidth,
		height : imgHeight
	};
}

exports.imageResize = function(obj) {
	var photoDir = Ti.Filesystem.applicationDataDirectory + Ti.Filesystem.separator + 'photos';

	var f = Ti.Filesystem.getFile(photoDir, obj.id);

	var blob = f.read();
	f = null;

	var rect = {
		width : blob.width,
		height : blob.height
	};
	var thumb = {
		width : obj.width
	};
	thumb.height = (rect.height / rect.width) * thumb.width;

	return blob.imageAsResized(thumb.width, thumb.height);
};

exports.cropImageMaintainRatio = function(filePath, cropWidth, cropHeight, returnBlob) {

	var dimensions = getImageDimensions(filePath);
	var baseWidth = dimensions.width;
	var baseHeight = dimensions.height;

	//If the cropping dimensions are more than the original image
	if (baseWidth <= cropWidth && baseHeight <= cropHeight) {
		var baseImage = Titanium.UI.createImageView({
			image : filePath,
			width : baseWidth + 'dp',
			height : baseHeight+ 'dp',
			left : '0dp',
			top : '0dp',
			_type : 'showcase'
		});
		if (returnBlob) {
			return baseImage.toImage();
		} else {
			return baseImage;
		}
	}

	var ratio = baseWidth / baseHeight;
	var scaledWidth;
	var scaledHeight;

	//Get the bigger dimension of the crop request, scale image based on the bigger dimension
	if (cropWidth === -1) {
		scaledHeight = cropHeight;
		scaledWidth = scaledHeight * ratio;
		cropWidth = scaledWidth;
	} else if (cropHeight === -1) {
		scaledWidth = cropWidth;
		scaledHeight = cropWidth / ratio;
		cropHeight = scaledHeight;
	} else {
		if (cropWidth > cropHeight) {
			scaledWidth = cropWidth;
			scaledHeight = scaledWidth / ratio;
		} else {
			scaledHeight = cropHeight;
			scaledWidth = scaledHeight * ratio;
		}
	}

	scaledHeight = parseInt(scaledHeight);
	scaledWidth = parseInt(scaledWidth);

	//Now crop the image at 0,0 cutting out the final imageview dimensions
	var croppedImage;
	if (isAndroid()) {
		var image = Ti.Filesystem.getFile(filePath).read();

		var resizedImage = image.imageAsResized(scaledWidth, scaledHeight);

		if (scaledWidth == cropWidth && scaledHeight == cropHeight) {
			croppedImage = resizedImage;
		} else {
			var imageDict = {
				x : 0,
				y : 0,
				width : cropWidth,
				height : cropHeight
			};
			croppedImage = resizedImage.imageAsCropped(imageDict);
		}
	} else {
		var imageView = Titanium.UI.createImageView({
			image : filePath,
			width : scaledWidth+ 'dp',
			height : scaledHeight+ 'dp',
			left : '0dp',
			top : '0dp'
		});
		var cropView = Titanium.UI.createView({
			width : cropWidth,
			height : cropHeight,
			left : '0dp',
			top : '0dp'
		});
		cropView.add(imageView);

		// now convert the crop-view to an image Blob
		croppedImage = cropView.toImage();
	}

	if (returnBlob) {
		return croppedImage;
	}
	// make an imageView containing the image Blob
	var imageView = Titanium.UI.createImageView({
		image : croppedImage,
		width : cropWidth+ 'dp',
		height : cropHeight+ 'dp',
		left : '0dp',
		top : '0dp',
		_type : 'showcase'
	});

	return imageView;
};

exports.createErrorMessage = function(err) {
	errorMessage = (err.error && err.message) || JSON.stringify(err);
	return errorMessage;
};

exports.isEmptyString = function(str) {
	if (str == '' || str == null || str == undefined) {
		return true;
	}
	return false;
};

exports.isAndroid = function() {
	return isAndroid();
};

exports.isiPhone = function() {
	return isiPhone();
};

exports.isiPad = function() {
	return isiPad();
};

exports.isMobileWeb = function() {
	return isMobileWeb();
};

exports.isiOs = function() {
	return isiOs();
};

exports.isTablet = function() {
	return isTablet();
};

function isiOs() {
	return (Ti.Platform.osname === 'iphone' || Ti.Platform.osname === 'ipad');
}

function isMobileWeb() {
	return (Ti.Platform.osname === 'mobileweb');
}

function isiPad() {
	return (Ti.Platform.osname === 'ipad');
}

function isiPhone() {
	return (Ti.Platform.osname === 'iphone');
}

function isAndroid() {
	return (Ti.Platform.osname === 'android');
}

function isTablet() {
	if (isAndroid()) {
		//If the screen is larger than 5 inches then it's a tab
		return Titanium.Platform.displayCaps.platformHeight / Titanium.Platform.displayCaps.dpi > 4 ? true : false;
	} else if (isiOs()) {
		return isiPad();
	}
}

exports.hideKeyboard = function() {
	if (isAndroid()) {
		Ti.UI.Android.hideSoftKeyboard();
	} else if (isiOs()) {
		//TODO textField.blur()
	}
};

exports.hasConnectivity = function() {
	return Titanium.Network.networkType != Titanium.Network.NETWORK_NONE;
};

var createDelimitedStr = function(names, delimiter) {
	var unique = {};
	var distinct = [];
	for (var i in names ) {
		var name = names[i];
		if ( typeof (unique[name]) == "undefined") {
			distinct.push(name);
		}
		unique[name] = 0;
	}
	var commaStr = distinct.join(delimiter);
	return commaStr;
};
exports.createCommaSeparatedStr = function(names) {
	return createDelimitedStr(names, ", ");
};

exports.createNewlineSeparatedStr = function(names) {
	return createDelimitedStr(names, "\n");
};

String.prototype.capitalize = function() {
	return this.charAt(0).toUpperCase() + this.slice(1);
};

String.prototype.toTitleCase = function() {
	var smallWords = /^(a|an|and|as|at|but|by|en|for|if|in|nor|of|on|or|per|the|to|vs?\.?|via)$/i;

	return this.replace(/[A-Za-z0-9\u00C0-\u00FF]+[^\s-]*/g, function(match, index, title) {
		if (index > 0 && index + match.length !== title.length && match.search(smallWords) > -1 && title.charAt(index - 2) !== ":" && (title.charAt(index + match.length) !== '-' || title.charAt(index - 1) === '-') && title.charAt(index - 1).search(/[^\s-]/) < 0) {
			return match.toLowerCase();
		}

		if (match.substr(1).search(/[A-Z]|\../) > -1) {
			return match;
		}

		return match.charAt(0).toUpperCase() + match.substr(1);
	});
};

var globals = require('globals');
var indicator = null;
var IPAD_TOAST_DURATION = 3000;

exports.hideLoadingIndicator = function(curWindow) {
	if (indicator != null) {
		if (globals.isiOs()) {
			indicator.closeIndicator();
		} else {
			//Couldn't get the loading dialog to work properly on Android so just using a toast.
			
			//indicator.hide();
			//indicator.visible = false;
			//curWindow.remove(indicator);
		}
	}
};

exports.showLoadingIndicator = function(args, curWindow) {
	indicator = createLoadingIndicator(args);

	if (globals.isiOs()) {
		indicator.openIndicator();
	} else {
		//Couldn't get the loading dialog to work properly on Android so just using a toast.
		var text = args.text || 'Loading ...';
		displayToast(text);
		//indicator.show();
		//curWindow.add(indicator);
	}
};

function createLoadingIndicator(args) {
	var width = 50, height = 50;

	var args = args || {};
	var text = args.text || 'Loading ...';

	function openIndicator() {
		if (globals.isiOs()) {
			win.open();
		}
		activityIndicator.show();
	}

	function closeIndicator() {
		activityIndicator.hide();
		if (globals.isiOs()) {
			win.close();
		}
	}

	if (globals.isAndroid()) {
		var activityIndicator = Ti.UI.createActivityIndicator({
			style : Ti.UI.ActivityIndicatorStyle.DARK,
			height : Ti.UI.FILL,
			width : 30,
			//message : text,
			color : 'green',
			font : {
				fontFamily : 'Helvetica Neue',
				fontSize : '26sp',
				fontWeight : 'bold'
			},
		});

		activityIndicator.openIndicator = openIndicator;

		activityIndicator.closeIndicator = closeIndicator;
		return activityIndicator;
	}

	var activityIndicator = Ti.UI.createActivityIndicator({
		style : Ti.UI.iPhone.ActivityIndicatorStyle.PLAIN,
		left : 0,
		height : Ti.UI.FILL,
		width : 30
	});

	var win = Titanium.UI.createWindow({
		height : height,
		width : width,
		borderRadius : 10,
		touchEnabled : false,
		backgroundColor : '#000',
		opacity : 0.6
	});

	var view = Ti.UI.createView({
		width : Ti.UI.SIZE,
		height : Ti.UI.FILL,
		center : {
			x : (width / 2),
			y : (height / 2)
		},
		layout : 'horizontal'
	});

	var label = Titanium.UI.createLabel({
		left : 10,
		width : Ti.UI.FILL,
		height : Ti.UI.FILL,
		//text : text,
		color : '#fff',
		font : {
			fontFamily : 'Helvetica Neue',
			fontSize : '16sp',
			fontWeight : 'bold'
		}
	});

	view.add(activityIndicator);
	//view.add(label);
	win.add(view);

	win.openIndicator = openIndicator;
	win.closeIndicator = closeIndicator;

	return win;
}
function displayToast (toast){
	if (globals.isAndroid()) {
		var toast = Ti.UI.createNotification({
			message : toast,
			duration : Ti.UI.NOTIFICATION_DURATION_LONG
		});
		toast.show();
	} else if (globals.isiOs()) {
		// window container
		var indWin = Titanium.UI.createWindow();

		//  view
		var indView = Titanium.UI.createView({
			height : Ti.UI.SIZE,
			width : Ti.UI.SIZE,
			borderRadius : 3,
			backgroundColor : '#000',
			opacity : .7
		});

		indWin.add(indView);

		// message
		var message = Titanium.UI.createLabel({
			text : toast,
			color : '#fff',
			width : Ti.UI.SIZE,
			height : Ti.UI.SIZE,
			textAlign : 'center',
			font : {
				fontFamily : 'Helvetica Neue',
				fontSize : '12sp',
				fontWeight : 'bold'
			}
		});

		indView.add(message);
		indWin.open();

		setTimeout(function() {
			indWin.remove(indView);
			indWin.close();
		}, IPAD_TOAST_DURATION);
	}
}
exports.displayToast = function(toast) {
	displayToast(toast);
};

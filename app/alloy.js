// START: APM service code injection
// Require the apm module
Alloy.Globals.apm = undefined;
try {
Alloy.Globals.apm = require("com.appcelerator.apm");
}
catch (e) {
Ti.API.info("com.appcelerator.apm module is not available");
}

// Initialize the module if it is defined
Alloy.Globals.apm && Alloy.Globals.apm.init();
// END: APM code injection

Alloy.Globals.networkIsOnline = false;
Alloy.Globals.networkType = '';

//  Popover as console output stuff
/*
Alloy.Globals.ConsoleOutputToPopover = true;

Alloy.Globals.ConsoleTextArea = Titanium.UI.createTextArea({
	editable:false,
	textAlign:Titanium.UI.TEXT_ALIGNMENT_LEFT,
	backgroundColor:"black",
	color:"green",
	top:"0",
	left:"0",
	height:Ti.UI.FILL,
	width:Ti.UI.FILL
});

Alloy.Globals.jkLog = function(logEntry){
	if(Alloy.Globals.ConsoleOutputToPopover){
		if(typeof logEntry === 'object'){
			Alloy.Globals.ConsoleTextArea.value += JSON.stringify(logEntry, null, "\t");
			Alloy.Globals.ConsoleTextArea.value += '\n';
		}
		else{
			Alloy.Globals.ConsoleTextArea.value += logEntry;
			Alloy.Globals.ConsoleTextArea.value += '\n';			
		}

	}
	else{
		console.log(logEntry);
	}
};

*/

// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};

//Android tablets are identified as handheld by default when using formFactor in tss
//Override the settings
if(OS_ANDROID)
{
	//If the screen is larger than 5 inches then it's a tab
	Alloy.isTablet =Titanium.Platform.displayCaps.platformHeight / Titanium.Platform.displayCaps.dpi > 4 ? true : false;
	Alloy.isHandheld = !Alloy.isTablet;
}
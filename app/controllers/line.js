var args = arguments[0] || {};

var horizontalLine = true;

if(args.horizontal){
	horizontalLine = true;
	$.containerId.top = 0;
	$.containerId.left = 0;
	$.containerId.width = '100%';
	$.containerId.height = 1;	
}
else {
	horizontalLine = false;
	$.containerId.top = 0;
	$.containerId.left = 0;
	$.containerId.width = 1;
	$.containerId.height = '100%';
}

if (args.backgroundColor) {
	$.containerId.backgroundColor = args.backgroundColor;
} else {
	//Default color
	$.containerId.backgroundColor = '#ff0000';
}


if (args.left != null) {
	$.containerId.left = args.left;
}

if (args.right != null) {
	$.containerId.right = args.right;
}

if (args.width != null) {
	$.containerId.width = args.width;
}


if (args.bottom != null || args.bottom == 0) {
	$.containerId.bottom = args.bottom;
}

if (args.top != null) {
	$.containerId.top = args.top;
}

if (args.zIndex != null) {
	$.containerId.zIndex = args.zIndex;
}




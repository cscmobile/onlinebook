var args = arguments[0] || {};

//Iterate through the style sheet elements,  creating the sections and their associated tiles.


if(args.top){
	$.tiledMenu.top = args.top;	
}

if(args.left){
	$.tiledMenu.left = args.left;	
}

if(args.bottom){
	$.tiledMenu.bottom = args.bottom;	
}

if(args.right){
	$.tiledMenu.right = args.right;	
}

$.tiledMenu.backgroundColor = 'transparent';
//$.tiledMenu.backgroundColor = 'green';


// Add the sections
if(args.tilesData){
	_.each(args.tilesData, function(tile){
		
		var menuTileContainerController = Alloy.createController('menuTileContainer', tile);
		var menuTileContainerView = menuTileContainerController.getView();
		
		$.tiledMenu.add(menuTileContainerView);
	});
	
}
var fileManager = require('fileManager');


function showGlobalPass() {
	var globalPassWindow = $.globalPassLogonId.getView();
	globalPassWindow.open(
		//globals.getAnimationDict()
	);
}

function hideGlobalPass() {
	var globalPassWindow = $.globalPassLogonId.getView();
	globalPassWindow.close();
}

/*
function flipConsole(){
    var popover = Alloy.createController('consolePopover').getView();
    popover.show({view:$.console});	
}
*/

Ti.App.addEventListener('logonSuccess', function(e) {
	hideGlobalPass();
});



Alloy.Globals.navWin = $.navigationControl;
$.navigationControl.open();

// SIMPLY UNCOMMENT THIS LINE TO GET FULL GLOBAL PASS LOGON INTEGRATION.
//showGlobalPass();


// Set up the root menu settings for first time in.
var rootMenuMetaData = {tagName:'index', fileType:'menudata', version:1, fileName:'index.json'};


fileManager.getMenuData(rootMenuMetaData, function(menuData){buildMenu(menuData);});


function buildMenu(menuData){

	var tilesMenuView = Alloy.createController('tiledMenu', menuData.menu).getView();
	$.tiledMenuContainer.add(tilesMenuView);
	
	//var tilesMenuView = $.menuOfTiles.getView();
	
	tilesMenuView.addEventListener('click', function(e){
		var selectionsMetaData = {tagName:e.source.tagName, fileType:e.source.fileType, version:e.source.version, fileName:e.source.fileName,};
		var win = null;
	
		win = Alloy.createController('genericMenu', selectionsMetaData).getView();
		Alloy.Globals.navWin.openWindow(win,{animate:true});
	});	
}



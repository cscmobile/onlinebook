var args = arguments[0] || {};
var translucent = arguments[1] || false;


var solidBackgroundColor = '#00000000';
var vOpacity = '88';


if(!(args.backgroundColor === '' && args.backgroundColor.length === 9)){

	solidBackgroundColor = '#' + vOpacity + args.backgroundColor.substring(3);
	$.background.backgroundColor = solidBackgroundColor;
}

if(!(args.icon === '')){
	$.icon.backgroundImage = args.icon;
}

if(!(args.backgroundColor === '')){
	$.colorLayer.backgroundColor = args.backgroundColor;
}

if(args.font){
	$.tileText.font = args.font;
}

if(!(args.title === '')){
	$.tileText.text = args.title;
}


if(!(args.padTop === 0 || args.padTop === '')){
	$.menuTile.top = args.padTop;
}


if(!(args.padLeft === 0 || args.padLeft === '')){
	$.menuTile.left = args.padLeft;
}

if(!(args.padBottom === 0 || args.padBottom === '')){
	$.menuTile.bottom = args.padBottom;
}

if(!(args.padRight === 0 || args.padRight === '')){
	$.menuTile.right = args.padRight;
}

//Ti.API.info(args);



// These values are how the event listener in the view that holds the manu, knows which tile has been clicked. And what to do with it (json or pdf etc)


if(!(args.menuMetaData === '')){
	// Cannot associate a json object with a view ($.colorLayer.menuMetaData = args.menuMetaData),  only literal values so have to put individual bits of metaData into separate locations.
	$.colorLayer.tagName = args.menuMetaData.tagName;
	//$.colorLayer.category = args.menuMetaData.category;
	$.colorLayer.fileType = args.menuMetaData.fileType;
	$.colorLayer.version = args.menuMetaData.version;
	$.colorLayer.fileName = args.menuMetaData.fileName;
}


// TODO: need to use fileType = 'pdf' or menudata  here from the metadata really!!
if(typeof args.docPath != 'string'){
	args.docPath = '';	
}

//$.colorLayer.docPath = args.docPath;
$.colorLayer.docPath = '' + args.menuMetaData.fileName;

if(!(args.webPath === '')){
	$.colorLayer.webPath = args.webPath;
}






var args = arguments[0] || {};

var ui_notifications = require('ui_notifications');



var docPath = 'notthisquarter.png';
var webPath = "https://www.c3.csc.com";


	console.log("In iOSDocumentViewer");
	console.log(args.docPath);
if(args.docPath != ''){

	docPath = args.docPath;
}
else{
	// Draw the box with the message in it
	var messageView = Alloy.createController('messageView').getView();
	$.iOSdocViewer.add(messageView);
}



if(args.webPath){
	webPath = args.webPath;
}

$.iOSdocViewer.url = docPath;
$.iOSdocViewer.show();

function changeLink(){
	if(messageView){
		$.iOSdocViewer.remove(messageView);
	}
	showIndicator();
	//Alloy.Globals.igscWebView.url = webPath;	
	//Alloy.Globals.igscWebView.url = 'https://www.c3.csc.com';		
}

function hideIndicator() {
	ui_notifications.hideLoadingIndicator($.movieViewer);
}

function showIndicator() {
	$.linkBtn.enabled = false;
	var args = {
		text : 'Loading...'
	};
	ui_notifications.showLoadingIndicator(args, $.movieViewer);
}


$.iOSdocViewer.addEventListener('load', function(e) {
	hideIndicator();
});

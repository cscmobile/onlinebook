var args = arguments[0] || {};

var ui_notifications = require('ui_notifications');



var docPath = 'notthisquarter.png';
var webPath = "https://www.c3.csc.com";

// recyling web view and a bonus is its already logged into c3 !!
Alloy.Globals.igscWebView.width = Ti.UI.FILL;
Alloy.Globals.igscWebView.height = Ti.UI.FILL;

// Add the global web view that was used for loging onto Global PAss.  As it has a  c3 session already set up.
$.pdfViewer.add(Alloy.Globals.igscWebView);

if(args.docPath != ''){
	docPath = args.docPath;
}
else{
	// Draw the box with the message in it
	var messageView = Alloy.createController('messageView').getView();
	$.pdfViewer.add(messageView);
}



if(args.webPath){
	webPath = args.webPath;
}

Alloy.Globals.igscWebView.url = docPath;

function changeLink(){
	if(messageView){
		$.pdfViewer.remove(messageView);
	}
	showIndicator();
	Alloy.Globals.igscWebView.url = webPath;	
	//Alloy.Globals.igscWebView.url = 'https://www.c3.csc.com';		
}

function hideIndicator() {
	//$.linkBtn.enabled = true;
	ui_notifications.hideLoadingIndicator($.pdfViewer);
}

function showIndicator() {
	$.linkBtn.enabled = false;
	var args = {
		text : 'Loading...'
	};
	ui_notifications.showLoadingIndicator(args, $.pdfViewer);
}


Alloy.Globals.igscWebView.addEventListener('load', function(e) {
	hideIndicator();
});
